/**
 * 
 */
package com.jagmeet.controller;

import java.util.List;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.jagmeet.model.ProductMV;
import com.jagmeet.model.ProductVM;
import com.jagmeet.service.ProductService;

/**
 * @author jagmeet
 *
 */
@RestController
@RequestMapping("/api")
public class ProductController {

	private static final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	/***
	 * API to retrieve all products
	 */
	@GetMapping("/products")
	public List<ProductMV> getAllProducts() {
		log.info("Entered in Controller");
		return productService.getAllProducts();
	}

	/***
	 * API to add a product
	 */
	@PostMapping("/addproduct")
	public ProductMV addProduct(@Valid @RequestBody ProductVM productVM) {
		log.info("Entered in Controller");
		return productService.addProduct(productVM);
	}

	/***
	 * API to fetch product by name
	 */
	@GetMapping("/products/search")
	public List<ProductMV> getProductByName(@RequestParam(value = "name", required = false) String productName) {
		log.info("Entered in Controller");
		return productService.getProductByName(productName);
	}

	/***
	 * API to fetch expired products
	 */
	@GetMapping("/products/expired")
	public List<ProductMV> getExpiredProducts() {
		log.info("Entered in Controller");
		return productService.getExpiredProducts();
	}

	/***
	 * API to fetch not expired products
	 */
	@GetMapping("/products/notexpired")
	public List<ProductMV> getUnexpiredProducts() {
		log.info("Entered in Controller");
		return productService.getUnexpiredProducts();
	}

	/***
	 * API to fetch products with a price range
	 */
	@GetMapping("/products/pricerange")
	public List<ProductMV> getProductsBetweenPriceRange(@RequestParam(value = "min", required = true) double min,
			@RequestParam(value = "max", required = false) double max) {
		log.info("Entered in Controller");
		return productService.getProductsBetweenPriceRange(min, max);
	}

	/***
	 * API to fetch products expiring in current month
	 */
	@GetMapping("/products/currentmonthexpired")
	public List<ProductMV> getExpiredProductCurrentMonth() {
		log.info("Entered in Controller");
		return productService.getExpiredProductCurrentMonth();
	}

}
