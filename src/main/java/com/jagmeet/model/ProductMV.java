/**
 * 
 */
package com.jagmeet.model;
/**
 * @author jagmeet
 *
 */
public class ProductMV {

	private Long productId;
	private String productName;
	private String productDescription;
	private Double productPrice;
	private java.sql.Date productExpiryDate;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public java.sql.Date getProductExpiryDate() {
		return productExpiryDate;
	}

	public void setProductExpiryDate(java.sql.Date productExpiryDate) {
		this.productExpiryDate = productExpiryDate;
	}
}
