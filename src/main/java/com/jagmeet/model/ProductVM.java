/**
 * 
 */
package com.jagmeet.model;

import javax.validation.constraints.NotNull;

/**
 * @author jagmeet
 *
 */
public class ProductVM {

	@NotNull
	private String productName;
	@NotNull
	private String productDescription;
	@NotNull
	private Double productPrice;
	@NotNull
	private java.sql.Date productExpiryDate;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public java.sql.Date getProductExpiryDate() {
		return productExpiryDate;
	}

	public void setProductExpiryDate(java.sql.Date productExpiryDate) {
		this.productExpiryDate = productExpiryDate;
	}
}
