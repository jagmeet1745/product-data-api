/**
 * 
 */
package com.jagmeet.service;

import java.util.List;

import com.jagmeet.model.ProductMV;
import com.jagmeet.model.ProductVM;

/**
 * @author jagmeet
 *
 */
public interface ProductService {

	public List<ProductMV> getAllProducts();

	public ProductMV addProduct(ProductVM productVM);

	public List<ProductMV> getProductByName(String productName);

	public List<ProductMV> getExpiredProducts();

	public List<ProductMV> getProductsBetweenPriceRange(double price1, double price2);

	public List<ProductMV> getUnexpiredProducts();

	public List<ProductMV> getExpiredProductCurrentMonth();

}
