/**
 * 
 */
package com.jagmeet.service.impl;

import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jagmeet.entity.Product;
import com.jagmeet.model.ProductMV;
import com.jagmeet.model.ProductVM;
import com.jagmeet.repository.ProductRepository;
import com.jagmeet.service.ProductService;

/**
 * @author jagmeet
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

	private static final Logger log = LoggerFactory.getLogger(ProductService.class);
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<ProductMV> getAllProducts() {
		log.info("Entered in Service");
		List<Product> products = (List<Product>) productRepository.findAll();
		ProductMV[] productMVs = modelMapper.map(products, ProductMV[].class);
		return Arrays.asList(productMVs);
	}

	@Override
	public ProductMV addProduct(ProductVM productVM) {
		log.info("Entered in Service");
		modelMapper.getConfiguration()
		  .setMatchingStrategy(MatchingStrategies.STRICT);
		Product product = modelMapper.map(productVM, Product.class);
		productRepository.save(product);
		ProductMV productMV = modelMapper.map(product, ProductMV.class);
		return productMV;
	}

	public List<ProductMV> getProductByName(String productName) {
		log.info("Entered in Service");
		List<Product> products;
		if (productName != null) {
			products = productRepository.findByproductName(productName);
		} else {
			// Condition to fetch whole data if search input is null
			products = (List<Product>) productRepository.findAll();

		}
		ProductMV[] productMVs = modelMapper.map(products, ProductMV[].class);
		return Arrays.asList(productMVs);

	}

	@Override
	public List<ProductMV> getExpiredProducts() {
		log.info("Entered in Service");
		List<Product> products = productRepository.getExpiredProducts();
		ProductMV[] productMVs = modelMapper.map(products, ProductMV[].class);
		return Arrays.asList(productMVs);
	}

	@Override
	public List<ProductMV> getProductsBetweenPriceRange(double min, double max) {
		log.info("Entered in Service");
		List<Product> products = productRepository.findByproductPriceBetween(min, max);
		ProductMV[] productMVs = modelMapper.map(products, ProductMV[].class);
		return Arrays.asList(productMVs);
	}

	@Override
	public List<ProductMV> getUnexpiredProducts() {
		log.info("Entered in Service");
		List<Product> products = productRepository.getUnexpiredProducts();
		ProductMV[] productMVs = modelMapper.map(products, ProductMV[].class);
		return Arrays.asList(productMVs);
	}

	@Override
	public List<ProductMV> getExpiredProductCurrentMonth() {
		log.info("Entered in Service");
		List<Product> products = productRepository.getExpiredProductCurrentMonth();
		ProductMV[] productMVs = modelMapper.map(products, ProductMV[].class);
		return Arrays.asList(productMVs);
	}

}
