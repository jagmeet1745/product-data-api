/**
 * 
 */
package com.jagmeet.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jagmeet.entity.Product;

/**
 * @author jagmeet
 *
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

	List<Product> findByproductName(String productName);

	@Query(value = "Select * from product where product_expiry_date < curdate();", nativeQuery = true)
	List<Product> getExpiredProducts();

	@Query(value = "Select * from product where product_expiry_date > curdate();", nativeQuery = true)
	List<Product> getUnexpiredProducts();

	List<Product> findByproductPriceBetween(double min, double max);

	@Query(value = "select * from product where extract(month from now())=extract(month from product_expiry_date) and year(now())=year(product_expiry_date);", nativeQuery = true)
	List<Product> getExpiredProductCurrentMonth();
}
